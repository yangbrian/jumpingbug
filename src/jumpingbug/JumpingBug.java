package jumpingbug;

import info.gridworld.grid.*;
import info.gridworld.actor.*;

/**
 * Jumping Bugs acts like a normal bug, 
 * however when they encounter another bug or rock, 
 * they try to jump over that obstacle into the cell 
 * immediately on the other side.<br/>
 * AP Computer Science Project 3 Part 1
 * @author Brian Yang
 * @version 04.06.2013
 */
public class JumpingBug extends Bug {
    
    /**
     * Can the Jumping Bug jump over the obstacle?
     * @return a boolean indicating whether or not the bug can jump over the adjacent obstacle
     */
    public boolean canJump() {
        // is the bug even on a grid?
        Grid<Actor> gr = getGrid();
        if (gr == null)
            return false;
        
        // get the current location, the next adjacent location, and the next adjacent location after that
        // this is where the bug would jump to if it could jump
        Location jumpOver = getLocation().getAdjacentLocation(getDirection()).getAdjacentLocation(getDirection());
        
        // is the jump to location valid?
        if (!gr.isValid(jumpOver))
            return false;
        
        // is it occupied by a bug or rock?
        Actor otherNeighbor = gr.get(jumpOver);
        return (otherNeighbor == null) || (otherNeighbor instanceof Flower);
    }
    
    /**
     * Makes the Jumping Bug jump over the adjacent obstacle 
     * and like a normal bug, leave a flower behind
     */
    public void jump() {
        // is the bug even on a grid?
        Grid<Actor> gr = getGrid();
        if (gr == null)
            return;
        
        // the bug's current location
        Location loc = getLocation();
        // the location it would jump to
        Location jumpOver = loc.getAdjacentLocation(getDirection()).getAdjacentLocation(getDirection());
    
        // is the new location valid? if so, move to it
        if(gr.isValid(jumpOver))
            moveTo(jumpOver);
        else
            removeSelfFromGrid();
        
        // leave a flower of the same color behind
        Flower flower = new Flower(getColor());
        flower.putSelfInGrid(gr, loc);
    }
    
    /**
     * Called when the Act button is clicked<br/>
     * Jumping Bug moves, turns, or jumps
     */
    @Override
    public void act() {
        if(canMove())
            move(); // the bug can move so just act like a normal bug then
        else if(canJump())
            // the bug can't move normally
            // can it jump then?
            jump(); // then jump!
        else
            // so act like a normal bug
            // can't move or jump so turn 45 degrees to the right
            turn();
    }
}
